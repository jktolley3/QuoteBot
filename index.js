//Initialize some stuff, and import my information
const Discord = require('discord.js');
const bot = new Discord.Client();
const settings = require("./settings.json");
var list = require("./quotes.json");
bot.login(settings.token);
var prefix = settings.prefix;
var incorrectQuotes = new Array();
var totalQuotes = objectLength(list.quotes);
var count = 0;

//On boot
bot.on('ready', () => {

    console.log('The bot is online!');
    console.log("We have a total of " + totalQuotes + " potential quotes...")
    checkList(list);
    removeQuote(list, incorrectQuotes);
    console.log('Therefore we have a total of ' + (totalQuotes - incorrectQuotes.length) + ' quotes that are correctly formatted.');

});

//Check to see if the comamand is one we're looking for!
function commandIs(str, msg) {

    if(msg.toLowerCase() == prefix + str || msg.toLowerCase() == prefix + str + "s") {

        return true;

    }

    return false;

}

//See how big an object is
function objectLength( object ) {
    var length = 0;
    for( var key in object ) {
        if( object.hasOwnProperty(key) ) {
            ++length;
        }
    }
    return length;
}

//Makes sure all of our quotes are defined correctly.
function checkList (list) {

    for(i = 0; i < totalQuotes; i++) {

        if (objectLength(list.quotes[i]) != 3) {

            console.log("Quote number " + (i + 1) + " isn't filled out correctly.");
            incorrectQuotes.push(i);

        } else if(list.quotes[i].quote == undefined || list.quotes[i].from == undefined || list.quotes[i].id == undefined) {

            console.log("Quote number " + (i + 1) + " isn't filled out correctly.");
            incorrectQuotes.push(i);

        }

    }
}

function removeQuote (totalList, incorrectQuotes) {

    for(i = 0; i < incorrectQuotes.length; i++) {

        totalList.quotes[incorrectQuotes[i]] = {

            "quote": "Not formatted Correctly:",
            "from": "The Programmer",
            "id": "youGoofed"

        }


    }

}

//Choose a Random Quote
function randomQuote(list) {
    
    var random = Math.random();
    var total = objectLength(list.quotes);
    var spread = 1/total;
    var count = 0;

    for(i = spread; i <= 1; i = i + spread) {

        if((i-spread) < random  && random <= i) break;
        else count++;

    }
    
    var quote = list.quotes[count];
    return quote;
}

function idExist(list, id) {

     if(findQuote(list, id) !== -1) {

        return true;

     } else {

        return false;

     }
    

}

function findQuote(list, id) {

    var found = -1;
    for(i = 0; i < objectLength(list.quotes); i++ ) {

        if(list.quotes[i].id.toLowerCase() == id.toLowerCase()) {

            found = i;

        }

    }

    if(found != -1) {

        return list.quotes[found];

    } else {
 
        return found;

    }

}

bot.on('message', (message) => {

    if(message.author.bot) return;
    if(!message.content.startsWith(prefix)) return;

    var args = message.content.split(" ");

    if(commandIs("random", args[0])) {

        var quote = randomQuote(list);
        message.channel.sendMessage(JSON.stringify(quote.quote) + " - " + quote.from);

    } else if(commandIs("quote", args[0])) {

        if(args.length != 2) {

            message.channel.sendMessage("You have specified an incorrect number of arguments.  The correct usage is: `" + prefix + "quote (ID)`");

        } else {

                if(!idExist(list, args[1])) {

                    message.channel.sendMessage("Quote ID not found in quotes.json");

                } else {

                    var quote = findQuote(list, args[1]);
                    message.channel.sendMessage(JSON.stringify(quote.quote) + " - " + quote.from);

                }

        }

    } else {

        return;

    }
}); 